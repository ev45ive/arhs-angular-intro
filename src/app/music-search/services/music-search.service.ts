import { Injectable, Inject, EventEmitter } from "@angular/core";
import { Album, AlbumsResponse } from "../../../models/Album";
import { MUSIC_API_URL } from "../tokens";

export class MyError extends Error {
  message: "Broken!";
}

// export abstract class BaseClass{}
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";

@Injectable({
  // providedIn:MusicSearchModule
  providedIn: "root"
})
export class MusicSearchService {

  requestSearch(params: { type: string; q: string }) {
    return this.http
      .get<AlbumsResponse>(this.api_url, {
        params
      })
      .pipe(
        map(response => response.albums.items),
        catchError(error => {
          this.errorChanges.next(error);
          return empty();
        })
      );
  }
  constructor(
    @Inject(MUSIC_API_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    this.queryChanges
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params => this.requestSearch(params))
      )
      .subscribe(albums => {
        this.albumsChanges.next(albums);
      });

    (window as any).albumsChanges = this.albumsChanges;
  }

  albumsChanges = new BehaviorSubject<Album[]>([]);
  queryChanges = new BehaviorSubject<string>("batman");
  errorChanges = new Subject<Error>();

  search(query: string) {
    this.queryChanges.next(query);
  }

  getAlbums() {
    return this.albumsChanges;
  }

  getQuery() {
    return this.queryChanges;
  }
}

import {
  pluck,
  map,
  catchError,
  startWith,
  switchAll,
  concatAll,
  mergeAll,
  switchMap
} from "rxjs/operators";
import {
  empty,
  throwError,
  of,
  concat,
  Subject,
  ReplaySubject,
  BehaviorSubject
} from "rxjs";
