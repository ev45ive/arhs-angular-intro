import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "../../../models/Playlist";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output()
  edit = new EventEmitter();

  onEdit() {
    this.edit.emit();
  }

  @Output()
  cancel = new EventEmitter();

  onCancel() {
    this.cancel.emit();
  }

  counter = 0;

  constructor() {
    // setInterval(() => {
    //   this.counter++;
    // }, 500);
  }

  ngOnInit() {}
}
