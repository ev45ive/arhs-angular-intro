// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// OR be professional - https://github.com/manfredsteyer/angular-oauth2-oidc :-)

export const environment = {
  production: false,
  music_search_url: "https://api.spotify.com/v1/search",
  authOptions: {
    auth_url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "3fe90a047975409c93d0287df906c6d9"
  } as AuthOptions
  
  // placki@placki.com
  // ******
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
import { AuthOptions } from "../app/security/auth.service";
