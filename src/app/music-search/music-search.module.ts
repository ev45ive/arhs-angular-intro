import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchViewComponent } from "./music-search-view/music-search-view.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { MUSIC_API_URL } from "./tokens";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MusicSearchRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [MusicSearchViewComponent],
  providers: [
    {
      provide: MUSIC_API_URL,
      useValue: environment.music_search_url
    }
    // {
    //   provide:HttpHandler,
    //   useClass:MySpecialMagicHandler
    // },
    // {
    //   provide: "MusicSearchService",
    //   useFactory: function(url /* ,a,b,c,d */) {
    //     return new MusicSearchService(url);
    //   },
    //   deps: [MUSIC_API_URL /* ,a,b,c,d */]
    // },
    // {
    //   provide: "MusicSearchService",
    //   useClass: MusicSearchService,
    //   // deps: [FAKE_MUSIC_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: SpotifySearchService,
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    // },
    // MusicSearchService
  ]
})
export class MusicSearchModule {}
