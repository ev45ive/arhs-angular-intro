import { Action, createAction } from "@ngrx/store";
import { Playlist } from "../models/Playlist";

export enum PlaylistsActionTypes {
  LoadPlaylists = "[Playlists] Load Playlists",
  UpdatePlaylist = "[Playlists] Update Playlist",
  CreatePlaylist = "[Playlists] Create Playlist",
  RemovePlaylist = "[Playlists] Remove Playlist",
  SelectPlaylist = "[Playlists] Select Playlist"
}

export class LoadPlaylists implements Action {
  readonly type = PlaylistsActionTypes.LoadPlaylists;
}

export class SelectPlaylist implements Action {
  readonly type = PlaylistsActionTypes.SelectPlaylist;
  constructor(readonly payload: Playlist["id"]) {}
}

export class CreatePlaylist implements Action {
  readonly type = PlaylistsActionTypes.CreatePlaylist;
}

export class UpdatePlaylist implements Action {
  readonly type = PlaylistsActionTypes.UpdatePlaylist;
  constructor(readonly payload: Playlist) {}
}

// export const CreatePlaylist = createAction(PlaylistsActionTypes.CreatePlaylist);

// export const UpdatePlaylist = createAction(
//   PlaylistsActionTypes.UpdatePlaylist,
//   (draft: Playlist) => ({ payload: draft })
// );

export type PlaylistsActions = LoadPlaylists | UpdatePlaylist | CreatePlaylist | SelectPlaylist;

// https://medium.com/ngrx/announcing-ngrx-version-8-ngrx-data-create-functions-runtime-checks-and-mock-selectors-a44fac112627
