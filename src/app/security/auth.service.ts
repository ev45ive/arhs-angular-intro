import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthOptions {
  auth_url: string;
  client_id: string;
  response_type: "token";
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private options: AuthOptions) {
    this.extractToken();
  }

  extractToken() {
    const jsonTOKEN = sessionStorage.getItem("token");
    this.token = JSON.parse(jsonTOKEN);

    if (this.token || !location.hash) {
      return;
    }

    const p = new HttpParams({
      fromString: location.hash
    });
    this.token = p.get("#access_token");

    if (this.token) {
      sessionStorage.setItem("token", JSON.stringify(this.token));
      location.hash = "";
    }
  }

  authorize() {
    const { auth_url, redirect_uri, response_type, client_id } = this.options;

    const p = new HttpParams({
      fromObject: {
        redirect_uri /* :redirect_uri */,
        response_type,
        client_id
      }
    });

    sessionStorage.removeItem("token");

    const url = `${auth_url}?${p.toString()}`;
    location.href = url;
    // console.log(url);
  }

  token: string | null = null;

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
