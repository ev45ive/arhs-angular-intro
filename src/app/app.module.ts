import { ApplicationRef } from "@angular/core";
import { AppComponent } from "./app.component";

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { PlaylistsModule } from "./playlists/playlists.module";
import { MusicSearchModule } from "./music-search/music-search.module";
import { SecurityModule } from "./security/security.module";
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { CounterComponent } from './counter/counter.component';
import { EffectsModule } from '@ngrx/effects';
import { MusicSearchEffects } from './music-search.effects';

@NgModule({
  declarations: [AppComponent, CounterComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlaylistsModule,
    MusicSearchModule,
    SecurityModule,
    StoreModule.forRoot(reducers, {
      metaReducers, 
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    EffectsModule.forRoot([MusicSearchEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app: ApplicationRef) {  }
  // ngDoBootstrap() {
  //   this.app.bootstrap(AppComponent, "app-root");
  // }
}
