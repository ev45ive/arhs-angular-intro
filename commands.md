ng g c playlists/playlists-view

ng g c playlists/items-list

ng g c playlists/list-item

ng g c playlists/playlist-details

ng g c playlists/playlist-form

# Global ngCLI

ng g c music-search -m app
ng g c music-search/music-search-view --export true

# Local ngCLI

npm run ng -- g c music-search/music-search-view --export true
npm run ng -- g c music-search/search-form
npm run ng -- g c music-search/albums-grid
npm run ng -- g c music-search/album-card

# Security
npm run ng -- g m security -m app
npm run ng -- g s security/auth
