import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "../../../models/Playlist";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistFormComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output()
  cancel = new EventEmitter();

  @Output()
  save = new EventEmitter();

  onSave(formValue) {
    this.save.emit({
      ...this.playlist,
      ...formValue
    });
  }

  onCancel() {
    this.cancel.emit();
  }

  // constructor(private cdr: ChangeDetectorRef) {
  //   cdr.detach();
  //   setInterval(()=>{
  //     cdr.detectChanges()
  //   },2000)
  // }

  ngOnInit() {}
}
