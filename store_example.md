```js
inc = (payload=1) => ({ type:'INC', payload }); 
dec = (payload=1) => ({ type:'DEC', payload });

[ inc(), inc(2), inc(), dec(2), dec() ].reduce( (state, action) => {
	switch(action.type){
        case 'INC': return { ...state, value: state.value + action.payload }
        case 'DEC': return { ...state, value: state.value - action.payload }
        default:
			return state
    }
},{
	value:0, other:1
})
```