import { Action } from "@ngrx/store";
import { Playlist } from "../models/Playlist";
import { PlaylistsActions, PlaylistsActionTypes } from "./playlists.actions";

export interface State {
  list: Playlist[];
  selectedId: Playlist["id"] | null;
}

export const initialState: State = {
  list: [],
  selectedId: null
};

export function reducer({
  state = initialState,
  action
}: {
  state: State;
  action: PlaylistsActions;
}): State {
  switch (action.type) {
    case PlaylistsActionTypes.UpdatePlaylist:
      return {
        ...state,
        list: state.list.map(p =>
          p.id == action.payload.id ? action.payload : p
        )
      };
    case PlaylistsActionTypes.SelectPlaylist:
      return {
        ...state,
        selectedId: action.payload || null
      };
    default:
      return state;
  }
}
