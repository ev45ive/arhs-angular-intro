import { Action } from "@ngrx/store";
import { Album } from "../models/Album";

export enum MusicSearchActionTypes {
  SearchAlbums = "[MusicSearch] Search Albums",
  AlbumsSearchStart = "[MusicSearch] Album Search Start",
  AlbumsSearchSuccess = "[MusicSearch] Album Search Success",
  AlbumsSearchFailure = "[MusicSearch] Album Search Failure"
}

export class SearchAlbums implements Action {
  readonly type = MusicSearchActionTypes.SearchAlbums;
  constructor(readonly query: string) {}
}

export class AlbumsSearchStart implements Action {
  readonly type = MusicSearchActionTypes.AlbumsSearchStart;
  constructor(readonly query: string) {}
}

export class AlbumsSearchSuccess implements Action {
  readonly type = MusicSearchActionTypes.AlbumsSearchSuccess;
  constructor(public payload: { data: Album[] }) {}
}

export class AlbumsSearchFailure implements Action {
  readonly type = MusicSearchActionTypes.AlbumsSearchFailure;
  constructor(public payload: { error: any }) {}
}

export type MusicSearchActions =
  | AlbumsSearchStart
  | AlbumsSearchSuccess
  | AlbumsSearchFailure;
