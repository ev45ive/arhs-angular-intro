import { Component, OnInit } from "@angular/core";
import { Playlist } from "../../../models/Playlist";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, share, tap, shareReplay } from "rxjs/operators";
import { PlaylistsService } from "../services/playlists.service";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  playlists = this.playlistsService
    .getPlaylists()
    .pipe(tap(playlists => (this.howMany = playlists.length)));

  howMany = 0;

  selected = this.route.paramMap.pipe(
    map(params => params.get("playlist_id")),
    map(id => parseInt(id)),
    switchMap(id => this.playlistsService.getPlaylist(id)),
    shareReplay()
  );

  constructor(
    private router: Router,
    private playlistsService: PlaylistsService,
    private route: ActivatedRoute
  ) {}

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id], {
      replaceUrl: true
    });
  }

  mode = "show";

  ngOnInit() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  save(draft: Playlist) {
    this.playlistsService.updatePlaylist(draft);
    
    this.mode = "show";
  }
}
