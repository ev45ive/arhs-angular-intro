import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { State } from "../reducers";
import { Increment, Decrement } from "../counter.actions";

@Component({
  selector: "app-counter",
  templateUrl: "./counter.component.html",
  styleUrls: ["./counter.component.scss"]
})
export class CounterComponent implements OnInit {
  constructor(private store: Store<State>) {}

  counter = this.store.select("counter");

  inc() {
    this.store.dispatch(new Increment(1));
  }

  dec() {
    this.store.dispatch(new Decrement(1));
  }

  ngOnInit() {}
}
