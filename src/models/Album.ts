export interface Image {
  url: string;
  height?: number;
  width?: number;
}

export interface Entity {
  id: string;
  name: string;
}

export interface Artist extends Entity {
  images: Image[];
  popularity: number;
  type: "artist";
}

export interface Album extends Entity {
  // type: "album";
  images: Image[];
  artists?: Artist[];
}

export interface PagingObject<T> {
  items: T[];
  limit?: number;
  next?: any;
  offset?: number;
  previous?: any;
  total?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
  // artists: PagingObject<Artist>;
}
