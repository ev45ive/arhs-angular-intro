import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";
import { LoginFormComponent } from "./login-form.component";
import { By } from "@angular/platform-browser";
import { LoginService } from "../login.service";
import { log } from "util";
import { FormsModule } from "@angular/forms";
import { Observable, of } from "rxjs";
import { EventEmitter } from "@angular/core";

fdescribe("LoginFormComponent", () => {
  let fixture: ComponentFixture<LoginFormComponent>;
  let component: LoginFormComponent;
  let FakeLoginService: jasmine.SpyObj<LoginService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginFormComponent],
      imports: [FormsModule],
      providers: []
    })
      .overrideProvider(LoginService, {
        useFactory: () => jasmine.createSpyObj<LoginService>(["login"])
      })
      .compileComponents();
  }));

  beforeEach(inject([LoginService], (service: jasmine.SpyObj<LoginService>) => {
    FakeLoginService = service;

    FakeLoginService.login.and.returnValue(of("failed"));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("is created", () => {
    expect(component).toBeTruthy();
  });

  it("renders message", () => {
    const element = fixture.debugElement.nativeElement;
    expect(element.innerText).toMatch("Please log in");
  });

  it("should display login input ", () => {
    const el = fixture.debugElement.query(By.css("input.login"));
    expect(el).not.toBeNull();
  });

  it("should display password input ", () => {
    const el = fixture.debugElement.query(
      By.css("input.password[type=password]")
    );
    expect(el).not.toBeNull();
  });

  it("clicking login button submits credentials", () => {
    const btn = fixture.debugElement.query(By.css("input[type=button]"));
    expect(btn).not.toBeNull();

    // the method Login() was called ?
    const loginSpy = spyOn(component, "login");

    btn.triggerEventHandler("click", {});

    // const event = new MouseEvent("click");
    // (btn.nativeElement as HTMLElement).dispatchEvent(event);

    expect(loginSpy).toHaveBeenCalled();
  });

  it("on login credentials are submitted to service", () => {
    component.login();
    expect(FakeLoginService.login).toHaveBeenCalled();
  });

  it("shows login details", async(() => {
    component.credentials.username = "admin";
    component.credentials.password = "pa55w0rd";

    // fixture.autoDetectChanges() + something.click()
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(findNativeElemByCss("input.login").value).toBe("admin");
      expect(findNativeElemByCss("input.password").value).toBe("pa55w0rd");
    });
  }));

  it("read credentials from user", () => {
    const login = findElemByCss("input.login");
    const password = findElemByCss("input.password");

    // login.nativeElement.value = "guest";
    password.nativeElement.value = "s3cr3t";

    login.triggerEventHandler("input", {
      target: { value: "guest" }
    });
    password.triggerEventHandler("input", {
      target: password.nativeElement
    });
    // login.nativeElement.dispatch(new Event('input'))
    // password.nativeElement.dispatch(new Event('input'))

    expect(component.credentials.username).toBe("guest");
    expect(component.credentials.password).toBe("s3cr3t");
  });

  it("shows message after succesful login ", () => {
    // Given / Prepare
    FakeLoginService.login.and.returnValue(of("success"));
    expect(component.message).not.toBe("success");

    // When / Do
    component.login();

    // Then / Verify
    expect(component.message).toBe("success");
  });

  it("shows message after unsuccesful login ", () => {
    const loginResult = new EventEmitter();

    FakeLoginService.login.and.returnValue(loginResult);
    expect(component.message).not.toBe("failed");

    component.login();
    expect(component.message).not.toBe("failed");

    loginResult.emit("failed");
    expect(component.message).toBe("failed");
  });

  function findElemByCss(selector) {
    return fixture.debugElement.query(By.css(selector));
  }

  function findNativeElemByCss(selector) {
    return findElemByCss(selector).nativeElement;
  }
});

// https://blog.thoughtram.io/angular/2016/12/27/angular-2-advance-testing-with-custom-matchers.html
