import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthService, AuthOptions } from "./auth.service";
import { environment } from "../../environments/environment";
import { LoginFormComponent } from "./login-form/login-form.component";
import { FormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  declarations: [LoginFormComponent],
  imports: [CommonModule, FormsModule],
  providers: [
    {
      provide: AuthOptions,
      useValue: environment.authOptions
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  exports: [LoginFormComponent]
})
export class SecurityModule {
  constructor(private auth: AuthService) {
    auth.getToken();
  }
}
