import { Action } from "@ngrx/store";
import { CounterActions, CounterActionTypes } from "./counter.actions";

export interface CounterState {
  counter: number;
}

export const initialState: CounterState = {
  counter: 0
};

export function reducer(
  state = initialState,
  action: CounterActions
): CounterState {

  switch (action.type) {
    case CounterActionTypes.Increment:
      return {
        ...state,
        counter: state.counter + action.payload
      };

    case CounterActionTypes.Decrement:
      return {
        ...state,
        counter: state.counter - action.payload
      };

    default:
      return state;
  }
}
