import { InjectionToken } from "@angular/core";

export const MUSIC_API_URL = new InjectionToken("Url for Music Search service");
