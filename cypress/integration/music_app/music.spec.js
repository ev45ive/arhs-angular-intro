describe("music search", () => {
  beforeEach(() => {
    cy.server();

    // cy.route("https://api.spotify.com/v1/search**", {
    //   albums: {
    //     items: [
    //       {
    //         id: "ot8g9eh4ioh7ge4",
    //         name: "Awesoem Album",
    //         images: [
    //           {
    //             url: "https://www.placecage.com/c/300/300"
    //           }
    //         ]
    //       }
    //     ]
    //   }
    // });

    cy.fixture("albums.json").as("albumsJSON");

    cy.route("https://api.spotify.com/v1/search**", "@albumsJSON");

    cy.visit("http://localhost:4200/music");
  });

  it("shows search form", () => {
    cy.get("input").type("batman", {
      delay: 100
    });
  });
});
