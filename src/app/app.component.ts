import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
  // template:`<h1>hello world</h1>`,
  // styles:[`h1{color:pink;}`]
})
export class AppComponent {
  title = 'arhs-angular awesome!';
}