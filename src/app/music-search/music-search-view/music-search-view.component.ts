import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicSearchService } from "../services/music-search.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  Subscription,
  Subject,
  ConnectableObservable,
  ReplaySubject
} from "rxjs";
import {
  takeUntil,
  multicast,
  refCount,
  share,
  shareReplay
} from "rxjs/operators";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"]
  // providers:[]
})
export class MusicSearchViewComponent implements OnInit {
  
  albumsChanges = this.service.getAlbums().pipe(share());
  queryChanges = this.service.getQuery();

  message: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {
    // (this.albumsChanges as ConnectableObservable<Album[]>).connect()
    // this.route.queryParamMap.subscribe(paramMap => {
    //   const query = paramMap.get("q");
    //   if (query) {
    //     this.query = query;
    //     this.service
    //       .getAlbums(query)
    //       .subscribe(
    //         albums => (this.albums = albums),
    //         error => (this.message = error.message)
    //       );
    //   }
    // });
  }

  // search(query: string) {
  //   this.router.navigate([], {
  //     queryParams: {
  //       q: query
  //     },
  //     replaceUrl: true,
  //     relativeTo: this.route
  //   });
  // }

  search(query: string) {
    this.service.search(query);
  }

  ngOnInit() {}

  ngDoCheck() {}
}
