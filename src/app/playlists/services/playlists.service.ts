import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { Playlist } from "../../../models/Playlist";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular HIts!",
      favourite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular Top 20!",
      favourite: true,
      color: "#ff0000"
    },
    {
      id: 345,
      name: "Best of Angular!",
      favourite: true,
      color: "#ffff00"
    }
  ]);

  getPlaylists() {
    return this.playlists.asObservable();
  }

  getPlaylist(id: number) {
    return this.playlists.pipe(
      map(items => items.find(p => p.id == id))
    );
  }

  createPlaylist() {
    const playlists = this.playlists.getValue();
    playlists.push({
      id: Date.now(),
      name: "",
      favourite: false,
      color: "#000000"
    });
    this.playlists.next(playlists);
  }

  updatePlaylist(draft: Playlist) {
    const playlists = this.playlists.getValue();

    const index = playlists.findIndex(p => p.id == draft.id);
    playlists.splice(index, 1, draft);

    this.playlists.next(playlists);
  }

  constructor() {}
}
