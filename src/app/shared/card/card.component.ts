import { Component, OnInit, ContentChildren } from "@angular/core";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"]
})
export class CardComponent implements OnInit {

  @ContentChildren("something")
  children: any[];

  constructor() {}

  ngOnInit() {}
}
