import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  ValidationErrors,
  AbstractControl,
  AsyncValidatorFn,
  FormArray
} from "@angular/forms";
import { filter, distinctUntilChanged, debounceTime } from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const isError = ("" + control.value).includes("batman");

    return isError
      ? {
          censor: { badword: "batman" }
        }
      : null;
  };

  asyncCensor: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    return Observable.create((observer: Observer<ValidationErrors | null>) => {
      // return this.http.get(..).pipe(map(resps => erorr))

      // Subscribe:
      const handler = setTimeout(() => {
        const validation = this.censor(control);
        observer.next(validation);
        observer.complete();
      }, 2000);

      // onUnsubscribe:
      return () => {
        clearTimeout(handler);
      };
    });
  };

  queryForm = new FormGroup({
    query: new FormControl(
      "batman",
      [Validators.required, Validators.minLength(3) /* , this.censor */],
      [this.asyncCensor]
    ),
    options: new FormGroup({
      type: new FormControl("album")
    })
  });

  addMarket() {
    this.markets.push(new FormControl(""));
  }

  markets = this.queryForm.get("options.markets") as FormArray;

  @Input()
  set query(q: string) {
    this.queryForm.get("query").setValue(q, {
      emitEvent: false
      // onlySelf:true
    });
  }

  // ngOnChanges(inputs){
  //   console.log(inputs)
  //   this.queryForm.get('query').setValue(inputs.query.currentValue)
  // }

  constructor() {
    console.log(this.queryForm);
    // this.queryForm.valueChanges

    // this.queryForm.reset()

    this.queryForm
      .get("query")
      .valueChanges.pipe(
        debounceTime(400),
        filter(q => q.length >= 3),
        distinctUntilChanged()
      )
      // .subscribe(this.queryChange)
      .subscribe(next => this.emitSearch(next));
  }

  @Output()
  queryChange = new EventEmitter<string>();

  emitSearch(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
