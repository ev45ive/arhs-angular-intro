// export class Playlist{
// constructor(id, name, ..., ...)
// }

export interface Playlist extends Entity {
  favourite: boolean;
  /**
   * Hex Color please!
   */
  color: string;
  tracks?: Track[];
}

export interface Entity {
  id: number; // | string | null | any
  name: string;
}

export interface Track extends Entity {}

// tracks: Array<Track>;
// if(playlist.id !== null ){
//   playlist.id.toString()
// }
