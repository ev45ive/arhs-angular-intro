// CLone
git clone https://bitbucket.org/ev45ive/arhs-angular-intro.git
cd arhs-angular-intro

// Install
npm i
npm i -g @angular/cli
ng --version

// RUN
npm run serve


// NGRX:
npm install @ngrx/store @ngrx/effects @ngrx/entity @ngrx/store-devtools @ngrx/schematics --save

ng generate @ngrx/schematics:store State --root --module app.module.ts
ng generate @ngrx/schematics:reducer User 
ng generate @ngrx/schematics:action User 

ng generate @ngrx/schematics:effect MusicSearch --root -mapp.module.ts