import { AuthOptions } from '../app/security/auth.service';

export const environment = {
  production: true,
  music_search_url: "https://api.spotify.com/v1/search",
  authOptions: {
    auth_url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "3fe90a047975409c93d0287df906c6d9"
  } as AuthOptions
};
