describe("app component", () => {
  beforeEach(() => {
    cy.visit("http://localhost:4200/");
  });

  it("shoud select playlist", () => {
    cy.contains("Angular Top 20").click();
    cy.contains("Name:")
      .next()
      .should("contain.text", "Angular Top 20");

    cy.get("[data-id]")
      .invoke("attr", "data-id")
      .then(id => {
        cy.url().should("include", id);
      });
  });

  it("should switch to edit mode", () => {
    selectPlaylist(1);
    cy.get('input[value="Edit"]').click();
  });

  function selectPlaylist(index = 1) {
    cy.get(".list-group-item")
      .eq(index)
      .click();
  }
});
