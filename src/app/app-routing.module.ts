import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists/playlists-view/playlists-view.component";
import { MusicSearchViewComponent } from "./music-search/music-search-view/music-search-view.component";
import { CounterComponent } from './counter/counter.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path:'counter',
    component:CounterComponent
  },
  {
    path: "playlists",
    component: PlaylistsViewComponent
  },
  {
    path: "playlists/:playlist_id",
    component: PlaylistsViewComponent
  },
  {
    path: "music",
    component: MusicSearchViewComponent
  },
  {
    path: "**",
    redirectTo: "playlists",
    pathMatch: "full"
  }
  // {
  //   path:'user',
  //   pathMatch:'prefix' // user   // user/1 // user/1/posts/3
  //   pathMatch:'full' // user
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
