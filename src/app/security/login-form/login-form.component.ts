import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"]
})
export class LoginFormComponent implements OnInit {
  message = "Please log in";
  
  credentials = {
    username: "",
    password: ""
  };

  login() {
    this.loginService.login().subscribe(result => {
      this.message = result;
    });
  }

  constructor(private loginService: LoginService) {}

  ngOnInit() {}
}
