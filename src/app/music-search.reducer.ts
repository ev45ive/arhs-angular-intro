import { Action } from "@ngrx/store";
import { Album } from "../models/Album";
import {
  MusicSearchActions,
  MusicSearchActionTypes
} from "./music-search.actions";

export interface State {
  results: Album[];
  query: string;
  error: any | null;
}

export const initialState: State = {
  results: [],
  query: "",
  error: null
};

export function reducer(
  state = initialState,
  action: MusicSearchActions
): State {
  switch (action.type) {
    case MusicSearchActionTypes.AlbumsSearchStart:
      return {
        ...state,
        query: action.query,
        error: null
      };
    case MusicSearchActionTypes.AlbumsSearchSuccess:
      return {
        ...state,
        results: action.payload.data
      };
    case MusicSearchActionTypes.AlbumsSearchFailure:
      return {
        ...state,
        error: action.payload.error
      };
    default:
      return state;
  }
}
