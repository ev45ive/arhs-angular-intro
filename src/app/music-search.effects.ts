import { Injectable } from "@angular/core";
import { Actions, Effect, createEffect, ofType } from "@ngrx/effects";
import { MusicSearchService } from "./music-search/services/music-search.service";
import {
  MusicSearchActionTypes,
  SearchAlbums,
  AlbumsSearchSuccess
} from "./music-search.actions";
import { switchMap, map, tap } from "rxjs/operators";

@Injectable()
export class MusicSearchEffects {
  constructor(private actions$: Actions, private music: MusicSearchService) {}

  searchMusic$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MusicSearchActionTypes.SearchAlbums),
      switchMap((action: SearchAlbums) => {
        return this.music.requestSearch({
          type: "album",
          q: action.query
        });
      }),
      map(albums => new AlbumsSearchSuccess({ data: albums }))
    );
  });
}
